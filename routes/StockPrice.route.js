const express = require('express');
const router = express.Router();
const multer = require('multer');
const path = require('path');
const { createStockPrice, searchSku } = require('../controllers/StockPrice.controller')



const storage = multer.diskStorage({
    destination: path.join(__dirname, '../public/docs'),
    filename: (req, file, cb, filename) => {
        console.log(file.originalname);
        cb(null, file.originalname );
    }
})

const upload = multer({storage})
const excel = upload.single('file');


router.route('/')
    .get(searchSku)
    .post(excel,createStockPrice);

module.exports = router;
