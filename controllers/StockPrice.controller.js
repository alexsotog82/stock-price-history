const StockPriceModel = require('../models/StockPrice.model');
const path = require('path');
const fs = require('fs');
const xlsx = require('xlsx');



exports.getAllStockPrice = async (req,res,next) => {
    try {
        const stock = await StockPriceModel.find();
        if(!stock) return res.status(400).json({
            success: false,
            message: 'No se pudo traer los stock'
        });

        return  res.status(200).json({
            success: true,
            count: stock.length,
            data: stock
        })
    } catch (error) {
        return res.status(500).json({
            success: false,
            message: 'No se pudo traer los stock'
        });
    }
}


exports.createStockPrice = async (req,res,next) => {
    try {
        const stockPrice = xlsx.readFile('./public/docs/stockPrice.xlsx');
        let workSheet = stockPrice.Sheets["Productos"] ;
        let listJson = xlsx.utils.sheet_to_json(workSheet);
        console.log(listJson.length, 'list');
        listJson.map(async (item) => {
            if(typeof(item.MercadoLibre) !== 'string') {
                const data = await StockPriceModel.create(item);
                return data
            }
        })

        return res.status(201).json({
            success: true,
            message: 'se esta guardado el archivo espere un momento'
        })
    } catch (error) {}
}


exports.searchSku = async (req,res,next) => {
    try {
        const stock = await StockPriceModel.find({ Ventiapp_1: req.params.sku });
        if(!stock) return res.status(404).json({
            success: false,
            message: 'no se encontro el sku'
        })

        return  res.status(200).json({
            success: true,
            data: stock
        })
    } catch (error) {
        return res.status(500).json({
            success: false,
            message: 'Problemas con el servidor conttacte al administrador'
        });
    }
}
